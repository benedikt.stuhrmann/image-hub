const mongoose = require("mongoose");
const Post = require("./models/post");
const fs = require("fs");

module.exports = {

    getPostsForPage: function (req, posts, items_per_page = 10) {
        let posts_per_page = items_per_page;
        let pages_total = Math.ceil(posts.length/posts_per_page);
        let page = (req.query.page && req.query.page != null && req.query.page > 0) ? req.query.page : 1 ;
        let from = (page - 1) * posts_per_page;
        let to = page * posts_per_page;

        let url = req.protocol + "://" + req.get("host") + req.originalUrl;

        let i_url = url.replace(new RegExp("&items=" + posts_per_page, 'g'), "");
        i_url = i_url.replace(new RegExp("/?items=" + posts_per_page, 'g'), "");
        i_url += (i_url.indexOf("?") == -1) ? "?" : "&";

        let p_url = url.replace(new RegExp("&page=" + page, 'g'), "");
        p_url = p_url.replace(new RegExp("/?page=" + page, 'g'), "");
        p_url += (p_url.indexOf("?") == -1) ? "?" : "&";

        return {
            info : {
                items:          posts_per_page,
                items_total:    posts.length,
                items_url:      i_url,
                from:           from,
                to:             to,
                page:           page,
                pages_total:    pages_total,
                pages_url:      p_url,
                prev_page:      (page - 1 > 0) ? page - 1 : null,
                prev_prev_page: (page - 2 > 0) ? page - 2 : null,
                next_page:      (page * 1 + 1 <= pages_total) ? page * 1 + 1 : null,
                next_next_page: (page * 1 + 2 <= pages_total) ? page * 1 + 2 : null
            },
            posts: posts.slice(from, to)
        };
    }, // end getPostsForPage

    imagePath: function (path) {
        path = path.replace(/\\+/g, "/");
        if (path.startsWith("resources/local_images")) {
            path = path.replace("resources/local_images", "/local_images");
        }
        return path;
    },

    orderQuery: function (order_string) {
        console.log(order_string);

        let order_by;
        switch (order_string) {
            case "created-desc":
                order_by = { created: -1 };
                break;
            case "created-asc":
                order_by = { created: 1 };
                break;
            case "updated-desc":
                order_by = { updated: -1 };
                break;
            case "updated-asc":
                order_by = { updated: 1 };
                break;
            case "likes-desc":
                order_by = { likes: -1 };
                break;
            case "likes-asc":
                order_by = { likes: 1 };
                break;
            default:
                order_by = { updated: -1 };
        }

        return order_by;
    }, // end orderQuery

    getFileExtension: function(filename) {
        return filename.split(".").pop();
    }, // end getFileExtension

    isSupportedExtension: function(extension) {
        let supported = false;

        switch (extension) {
            case "jpg":
                supported = true;
                break;
            case "jpeg":
                supported = true;
                break;
            case "png":
                supported = true;
                break;
            case "gif":
                supported = true;
                break;
            case "mp4":
                supported = true;
                break;
            case "webm":
                supported = true;
                break;
            case "ogg":
                supported = true;
                break;
        
        }

        return supported;
    }, // end isSupportedExtension

    /**
     * Creates a post object.
     * @param {*} title         Title of the post. Set to null for auto-generated title.
     * @param {*} reflink       Reflink of the post. Set to null, for no link.
     * @param {*} content       Content of the post (array of items).
     * @param {*} rotations     Rotations for the content. Comma separated
     * @param {*} tags          Array of hashtags for the post.
     */
    createPostObject: function(title, reflink, content, rotations, tags) {
        let post = new Post({
            _id:     null,
            title:   null,
            reflink: reflink,
            content: [],
            tags:    [],
            created: new Date(),
            updated: new Date()
        });

        post._id = new mongoose.Types.ObjectId();
        post.title = (title != null && title != "") ? title : "Post " + post.id;
        
        if (Array.isArray(tags)) {
            post.tags = tags;
        } else {
            tags.split(",").forEach(tag => {
                if (tag != "") post.tags.push(tag.trim().toLowerCase());
            });
        }

        if (!Array.isArray(content)) {
            content = content.split(",");
        }

        rotations = rotations.split(",");
        content.forEach( (link) => {
            console.log(link);
            if (link != "") {
                if (!link.startsWith("https") && !link.startsWith("http")) {
                    // local image
                    if (!link.startsWith("/local_images")) link = "/local_images/" + link;
                    if (post.tags.indexOf("local") == -1) post.tags.push("local"); // add "local" tag if it does not exist
                    if (post.tags.indexOf("external") != -1) post.tags = post.tags.slice(post.tags.indexOf("external"), 1); // remove "external" tag if it does exist
                } else {
                    // external image
                    if (post.tags.indexOf("external") == -1) post.tags.push("external"); // add "external" tag if it does not exist
                    if (post.tags.indexOf("local") != -1) post.tags = post.tags.slice(post.tags.indexOf("local"), 1); // remove "local" tag if it does exist
                }

                let content_obj = this.contentObj(link);

                let rotation = 1 * rotations[post.content.length];
                content_obj.rotate = rotation;

                post.content.push(content_obj);
            }
        });

        return post;
    }, // end createPostObject

    contentObj: function(link) {
        let content = {
            link: link,
            type: "",
            extension: "",
            rotate: 0
        };
    
        let extension = (/[.]/.exec(link)) ? /[^.]+$/.exec(link) : null;
        extension = extension[0].toLowerCase();
    
        switch(extension) {
            case "png" : 
                content.type = "image";
                content.extension = extension;
                break;
            case "jpg" :
                content.type = "image";
                content.extension = extension;
                break;
            case "jpeg" :
                content.type = "image";
                content.extension = extension;
                break;
            case "gif" :
                content.type = "image";
                content.extension = extension;
                break;
            case "webp" :
                content.type = "image";
                content.extension = extension;
                break;
            case "mp4" :
                content.type = "video";
                content.extension = extension;
                break;
            case "webm" :
                content.type = "video";
                content.extension = extension;
                break;
            case "ogg" :
                content.type = "video";
                content.extension = extension;
                break;
        }
    
        return content;
    }, // contentObj

    move: function(oldPath, newPath, callback) {
        
        // TODO can probably be simplified alot
        // check if dir(s) exists
        let path_parts = newPath.split("/");
        path_parts.length = path_parts.length - 1; // remove filename to get folder pathname
        
        let index = 0;
        let dir_path = path_parts[0];
        do {
            if (!fs.existsSync(dir_path)){
                fs.mkdirSync(dir_path, { recursive: true });
            } 
            index++;
            dir_path += "/" +  path_parts[index];
        } while(index <  path_parts.length);



        fs.rename(oldPath, newPath, function (err) {
            if (err) {
                if (err.code === 'EXDEV') {
                    copy();
                } else {
                    callback(err);
                }
                return;
            }
            callback();
        });
    
        function copy() {
            var readStream = fs.createReadStream(oldPath);
            var writeStream = fs.createWriteStream(newPath);
    
            readStream.on('error', callback);
            writeStream.on('error', callback);
    
            readStream.on('close', function () {
                fs.unlink(oldPath, callback);
            });
    
            readStream.pipe(writeStream);
        }
    }, // end move()

  };