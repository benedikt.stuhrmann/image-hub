const mongoose = require("mongoose");

const post_schema = mongoose.Schema({
    _id:      mongoose.Schema.Types.ObjectId,
    title:   String,
    reflink: String,
    content: [{
        link:      {type: String, required: true},
        type:      {type: String, required: true},
        extension: {type: String, required: true},
        rotate:    {type: Number, default: 0},
    }],
    tags:    {type: [String], required: true},
    likes:   {type: Number, default: 0},
    created: Date,
    updated: Date 
});

module.exports = mongoose.model("Post", post_schema);