$("document").ready( ()=> {

    // search field
    $("#search-string").on("change", (e) => {
        let search_string = e.target.value;
        search_string = search_string.replaceAll("#", "tag:");

        $.ajax({
            url:  "/search",
            method: "POST",
            data: {
                search: search_string
            }
        }).done( (res) => {
            document.location.href = res.redirect;
        });
    });

    // order-by field
    $("select.order-by").on("change", (e) => {
        let order = e.target.value;
        let query = document.location.search;

        if (!query || query == null || query == "") {
            // no query exists
            document.location.href += "?order-by=" + order;
        } else {
            // query exists
            if (query.indexOf("order-by") == -1) {
                // order-by does not exist
                document.location.href += "&order-by=" + order;
            } else {
                // order-by does exist
                let old_order = e.target.dataset.oldorder;
                document.location.href = document.location.href.replace("order-by=" + old_order, "order-by=" + order);
            }
        }
    });
    
    // scale image size according to slider value
    $(".slider.image-size").on("input", (e) => {
        let value = e.target.value;
        let perc = value / 100;
        let adjustedHeight = 700 * perc;

        let post = e.target.parentNode.parentNode.parentNode;
        let content_elements = post.querySelectorAll(".content-element");

        content_elements.forEach(element => {
            let media = element.querySelector("img, video");
            media.style.maxHeight = adjustedHeight + "px";

            let classes = media.classList;
            for (let i = 0; i < classes.length; i++) {
                if (classes[i].indexOf("rotate-") != -1) {
                    updateRotatedImages(media);
                }
            }
        });
    });

    // reset image size to standard size
    $(".reset-image-size").on("click", (e) => {
        // reset input field
        $(e.target.parentNode.parentNode).find("input[type='range']").val(100);

        // reset images
        let adjustedHeight = 700;
        let post = e.target.parentNode.parentNode.parentNode.parentNode;
        let content_elements = post.querySelectorAll(".content-element");

        content_elements.forEach(element => {
            let media = element.querySelector("img, video");
            media.style.maxHeight = adjustedHeight + "px";

            let classes = media.classList;
            for (let i = 0; i < classes.length; i++) {
                if (classes[i].indexOf("rotate-") != -1) {
                    updateRotatedImages(media);
                }
            }
        });
    });

    // play video, when hovering over it 
    $("video").on("mouseenter", (e) => {
        e.target.play();
    });

    // stop and reset video, when moving mouse away from the video 
    $("video").on("mouseleave", (e) => {
        e.target.pause();
        e.target.currentTime = 0;
    });

    // pause video, when pressing mouse over the video 
    $("video").on("mousedown", (e) => {
        e.target.pause();
        e.target.classList.add("hide-mouse");
        e.target.removeAttribute("controls");
    });

    // unpause video, when releasing mouse 
    $("video").on("mouseup", (e) => {
        e.target.play();
        e.target.classList.remove("hide-mouse");
        e.target.setAttribute("controls", "controls");
    });

    // set url for delete post popup modal
    $("#confirm-delete-post").on("show.bs.modal", (e) => {
        $("#confirm-delete").attr("href", $(e.relatedTarget).data("href"));
    });
    
    // scroll through post (left or right) when at the edge of the window
    var image_section;
    var scroll_amount;

    let fast = {
        border: 80,
        scroll_amount: 500
    };

    let normal = {
        border: 200,
        scroll_amount: 200
    };

    let slow = {
        border: 400,
        scroll_amount: 50
    };

    $(".image-section").on("mousemove", (e) => {
        image_section = $(e.target).closest(".image-section");
        let offset = image_section.offset();
        let mouse_xPos = e.pageX - offset.left;

        let width = $(document).width();
        
        if (mouse_xPos < fast.border) {
            // left - fast
            scroll_amount = "-=" + fast.scroll_amount;
            scrollPostContent();
        } else if (mouse_xPos < normal.border) {
            // left - normal
            scroll_amount = "-=" + normal.scroll_amount;
            scrollPostContent();
        } else if (mouse_xPos < slow.border) {
            // left - slow
            scroll_amount = "-=" + slow.scroll_amount;
            scrollPostContent();
        } else if (mouse_xPos > width - fast.border) {
            // right - fast
            scroll_amount = "+=" + fast.scroll_amount;
            scrollPostContent();
        } else if (mouse_xPos > width - normal.border) {
            // right - normal
            scroll_amount = "+=" + normal.scroll_amount;
            scrollPostContent();
        } else if (mouse_xPos > width - slow.border) {
            // right - slow
            scroll_amount = "+=" + slow.scroll_amount;
            scrollPostContent();
        } else {
            image_section.stop();
        }
    
    })
    .on("mouseleave", (e) => {
        $(e.target).closest(".image-section").stop(); 
    });

    /**
     * Scrolls the post content to the left or right
     */
    function scrollPostContent(){
        image_section.stop().animate({scrollLeft: scroll_amount}, 200, "linear", scrollPostContent);
    } // end scrollPostContent

    // fill likes heart on hover
    $(".heart").on("mouseover", (e) => {
        $(e.target).removeClass("far").addClass("fas");
    });

    // unfill likes heart when hover ends
    $(".heart").on("mouseleave", (e) => {
        if (!$(e.target).hasClass("liked")) {
            $(e.target).removeClass("fas").addClass("far");
        }
    });

    // like/unlike the post
    $(".heart").on("click", (e) => {
        let id = $(e.target.parentNode).closest(".post").data("postid");
        let like_elem = $(e.target.parentNode).find(".likes-count");
        let likes = like_elem.data("likes");
        let type;

        if (!$(e.target).hasClass("liked")) {
            // like the post
            $(e.target).removeClass("far").addClass("fas").addClass("liked");
            likes++;
            type = "like";
        } else {
            // unlike the post
            $(e.target).removeClass("fas").addClass("far").removeClass("liked");
            likes--;
            type = "unlike";
        }

        // update like count of post in db
        $.ajax({
            url:  "/like-post",
            method: "POST",
            data: {
                post_id: id,
                likes:   type
            }
        }).done( (res) => {
            like_elem.data("likes", res.likes);
            like_elem.text(res.likes);
        });
    });

    // apply correct styles to rotated images on the page on load
    let rotate = $(".rotate-0, .rotate-90, .rotate-180, .rotate-270, .rotate-360");
    for (let i = 0; i < rotate.length; i++) {
        updateRotatedImages(rotate[i]);
    }

}); // end document ready

/**
 * Adjusts styles of rotated images
 */
function updateRotatedImages(img) {
    let rotation = 0;
    let classes = img.classList;

    for (let i = 0; i < classes.length; i++) {
        if (classes[i].indexOf("rotate-") != -1) {
            rotation = classes[i].replace("rotate-", "");
            break;
        }
    }

    if (rotation == "0" || rotation == "360") {
        // standard rotation
        $(img).removeClass("rotate-0 rotate-180 rotate-270 rotate-360")
        $(img).css("left", "auto");
        $(img).closest("li").css("width", "auto");
    } else if (rotation == "180") {
        // flipped
        $(img).css("left", "auto");
        $(img).closest("li").css("width", "auto");
    } else {
        // turned 
        let scale_factor = 0.56;

        let img_height = $(img).height();
        let width = scale_factor * img_height;

        if (width == 0) width = 392;

        $(img).css("left", width);
        $(img).closest("li").css("width", width);
    }
    
} // end rotateImage


String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};