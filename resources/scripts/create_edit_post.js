$("document").ready( ()=> {

    // prevent form submit with enter
    $(document).on("keypress", ":input:not(#input-tags)", (e) => {
        return e.keyCode != 13;
    });

    $(document).on("change", "input[name='media-items']", async (e) => {
        let form_data = new FormData();
        Array.from($(e.target)[0].files).forEach(file => {
            form_data.append("media-items", file);
        });
        let response = await $.ajax({
            url:  "/create-post/upload-media",
            method: "POST",
            processData: false,
            contentType: false,
            enctype: "multipart/form-data",
            data: form_data
        });
        if (!Array.isArray(response.links)) return;
        response.links.forEach( (link) => {
            addToPreview(link);
        });
    });

    // setup sortable preview list
    $( () => {
        $(".content-preview ul").sortable({
            placeholder: "preview-placeholder",
            scroll : false
        });
        $(".content-preview ul").disableSelection();
    });

    // add images as preview
    $("input[name='content-link']").on("change", () => {
        let link = $("input[name='content-link']").val();
        $("input[name='content-link']").val("");

        addToPreview(link);        
    });

    // add link to preview list and clear input for next link
    $("input[name='content-link']").on("keypress", (e) => {
        if (e.keyCode == 13) {
            let link = $("input[name='content-link']").val();
            $("input[name='content-link']").val("");
        
            addToPreview(link);   
        }
    });

    const tagslist = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
          url: "/tagslist",
          filter: function(list) {
            return $.map(list, function(tag) {
              return { name: tag }; 
            });
          },
          cache: false
        }
      });
      tagslist.initialize();

    // allow enter for tagsinput and trim whitespaces
    $("#input-tags").tagsinput({
        confirmKeys: [13, 44, 32],
        trimValue: true,
        typeaheadjs: {
            name: 'tagslist',
            displayKey: 'name',
            valueKey: 'name',
            hint: true,
            highlight: true,
            autocomplete: true,
            minLength: 1,
            source: tagslist.ttAdapter()
        }
    });
    
    // auto select first option
    $('.twitter-typeahead').on('keydown', function(e) {
        if (e.which == 13) {
            if ($(".tt-suggestion:first-child").length) {
                e.preventDefault();
                // remove input
                $('.twitter-typeahead .tt-input').value;
                // auto select first option
                $(".tt-suggestion:first-child", this).trigger('click');
            }
        }
    });

    // load tags into tagsinput when in edit mode
    let loaded_tags = $("#edit-tags").val();
    if (loaded_tags != null && loaded_tags != "") {
        $("#input-tags").tagsinput("add", loaded_tags);
    }

    // delete image button
    $(".content-preview ul").on("click", "span.delete-content", (e) => {
        let li_element = e.target.parentNode.parentNode;
        if ($(li_element).hasClass("preview-element")) {
            li_element.remove();
            updateElementCounter();
        }
    });

    // rotate image button
    $(".content-preview ul").on("click", "span.rotate-content", (e) => {
        let element = $(e.target.parentNode.parentNode).find("img, video")[0];
        
        let classes = element.classList;
        let rotation = 0;
        for (let i = 0; i < classes.length; i++) {
            if (classes[i].indexOf("rotate-") != -1) {
                rotation = classes[i].replace("rotate-", "");
                $(element).removeClass(classes[i]);
            }
        }
        rotation = (rotation * 1) + 90;
        if (rotation != 0 && rotation != 360) {
            $(element).addClass("rotate-" + rotation);
        }
        updateRotatedImages(element);
    });

    // load directory
    $("#load-directory").on("click", (e) => {
        $("#confirm-load-dir").modal("hide");

        let dir = $("input[name='content-folder']").val();
        if (dir == undefined || dir == null || dir == "") return;

        $.ajax({
            url:  "/load-dir",
            method: "POST",
            data: {
                directory: dir
            }
        }).done( (res) => {
            if (!Array.isArray(res.links)) return;

            // remove old images/videos
            $(".content-preview ul").empty();

            // add new images/videos
            res.links.forEach( (link) => {
                addToPreview(link);
            });
        });
    });

    $("form#create-post").on("submit", () => {
        let preview_elements = $(".content-preview .preview-element .content");
        let links = [];
        let rotations = [];

        for (let i = 0; i < preview_elements.length; i++) {
            let element = preview_elements[i];
            let src = element.src;
            if (src.indexOf("localhost:3000") != -1) {
                src = src.replace("https://localhost:3000", "");
                src = src.replace("http://localhost:3000", "");
            } 
            links.push(src);

            let classes = element.classList;
            let rotation = 0;
            for (let j = 0; j < classes.length; j++) {
                if (classes[j].indexOf("rotate-") != -1) {
                    rotation = classes[j].replace("rotate-", "");
                }
            }
            rotations.push(rotation);
        }

        $("<input>").attr({
            type: "hidden",
            name: "content",
            value: links
        }).appendTo("form#create-post");

        $("<input>").attr({
            type: "hidden",
            name: "rotations",
            value: rotations
        }).appendTo("form#create-post");
    });

}); // end ready

function addToPreview(_link) {
    let link = _link.trim();
    if (!link || link == "") return;

    link = validateLink(_link);
    let content_type = contentType(link);

    if (content_type.type == "image") {
        $(".content-preview ul").append(
            $("<li>").addClass("preview-element ui-state-default").append([
                $("<span>").addClass("content-option rotate-content").append(
                    $("<i>").addClass("fas fa-redo")),
                $("<span>").addClass("content-option delete-content").append(
                    $("<i>").addClass("fas fa-times")),
                $("<img>").attr("src", link).attr("alt", link).addClass("content")
            ])
        );    
    } else if (content_type.type == "video") {
        let video_format = content_type.extension;

        $(".content-preview ul").append(
            $("<li>").addClass("preview-element ui-state-default").append([
                $("<span>").addClass("delete-content").append(
                    $("<i>").addClass("fas fa-times")),
                $("<video>").attr("controls", "true").append(
                    $("<source>").attr("src", link).attr("type", "video/" + video_format).addClass("content"))
            ])
        );    
    } else {
        console.log("Unknown file type");
    }

    updateElementCounter();

} // end addToPreview

function validateLink(link) {
    if (!link.startsWith("https") && !link.startsWith("http") && !link.startsWith("/local_images/")) {
        // no valid link => assume it's a local file and add the local images path to it
        if (link.indexOf("/") == 0) link = link.replace("/local_images/", "");
        if (link.indexOf("local_images/") == 0) link = link.replace("local_images/", "");

        let folder = $("input[name='content-folder']").val();
        if (folder != "") link = folder + "/" + link;
        
        link = "/local_images/" + link; 
    }

    return link;
} // end validateLink

function contentType(link) {
    let content_type = {
        type : null,
        extension : ""
    };

    let extension = (/[.]/.exec(link)) ? /[^.]+$/.exec(link) : null;
    extension = extension[0].toLowerCase();

    switch(extension) {
        case "png" : 
            content_type.type = "image";
            content_type.extension = extension;
            break;
        case "jpg" :
            content_type.type = "image";
            content_type.extension = extension;
            break;
        case "jpeg" :
            content_type.type = "image";
            content_type.extension = extension;
            break;
        case "gif" :
            content_type.type = "image";
            content_type.extension = extension;
            break;
        case "webp" :
            content_type.type = "image";
            content_type.extension = extension;
            break;
        case "mp4" :
            content_type.type = "video";
            content_type.extension = extension;
            break;
        case "webm" :
            content_type.type = "video";
            content_type.extension = extension;
            break;
        case "ogg" :
            content_type.type = "video";
            content_type.extension = extension;
            break;
    }

    return content_type;
} // end contentType

function updateElementCounter() {
    let preview_elements = $(".content-preview .preview-element .content").length;
    $(".element-counter span").html(preview_elements);
} // end updateElementCounter