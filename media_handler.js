const Post = require("./models/post");
const path = require("path");
const fs = require("fs/promises");

class MediaHandler {

    constructor() {
        this.uploadsDirectory = path.join(__dirname, "/resources/local_images/uploads/");
    }

    async getUploads() {
        let uploads = [];
        let files = await fs.readdir(this.uploadsDirectory);
        files.forEach(function (file) {
            let filePath = path.join(this.uploadsDirectory, file);
            uploads.push(this.imagePath(filePath));
        }.bind(this));
        return uploads;
    }

    async getUsedUploads() {
        let usedUploads = [];
        let posts = await Post.find().exec();
        posts.forEach( (post) => {
            post.content.forEach( (mediaItem) => {
                if (mediaItem.link.indexOf("/local_images/uploads") === -1) {
                    return;
                }
                if (usedUploads.includes(mediaItem.link)) {
                    return;
                }
                usedUploads.push(this.imagePath(mediaItem.link));
            });
        });
        return usedUploads;
    }


    async getUnusedUploads() {
        let uploads = await this.getUploads();
        let usedUploads = await this.getUsedUploads();
        let unusedUploads = uploads.filter(uploadedMedia => !usedUploads.includes(uploadedMedia));
        return unusedUploads;
    }

    async deleteUnusedUploads() {
        let unusedUploads = await this.getUnusedUploads();
        unusedUploads = unusedUploads.map(unusedUpload => this.imagePath(unusedUpload, true));
        console.log("Deleting " + unusedUploads.length + " unused uploads.");
        console.log(unusedUploads);
        this.deleteAll(unusedUploads);
    }

    deleteAll(media) {
        media.forEach(mediaItem => this.delete(mediaItem));
    }

    delete(media) {
        console.log("DELETE", media);
        fs.unlink(media)
    }

    imagePath (path, abs=false) {
        path = path.replace(/\\+/g, "/");
        let startIndex = path.indexOf("/local_images/");
        path = path.slice(startIndex);
        if (abs === true) {
            path = this.uploadsDirectory + path.replace("/local_images/uploads/", "")
        }
        return path;
    }

}

module.exports = MediaHandler;