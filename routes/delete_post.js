const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const path = require("path");
const Post = require("../models/post");

router.get("/", (req, res, next) => {

    let post_id = req.originalUrl.replace("/delete-post/", "");

    Post.remove({ _id: post_id})
        .exec()
        .then( () => {
            res.redirect("/");
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });
        
});

module.exports = router;