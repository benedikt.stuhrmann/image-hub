const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const path = require("path");
const Post = require("../models/post");
const utils = require("../utils");

router.get("/", (req, res, next) => {
    let search_string = req.query.search;
    let item_amount = (req.query.items && req.query.items != null) ? req.query.items : 10;
    let order = req.query["order-by"];
    let order_query = utils.orderQuery(order);

    Post.find({ title: new RegExp(search_string, "i")}) // find all posts that have the search string in their title (case-insensitive)
    .sort(order_query)
    .exec()
    .then(posts => {
        let posts_to_show = utils.getPostsForPage(req, posts, item_amount);
        res.render("index", {
            search_string: search_string,
            page_info:     posts_to_show.info,
            posts:         posts_to_show.posts,
            order:         order
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({error: err});
    });

});

router.post("/", (req, res, next) => {
    let search_string = req.body.search.trim();
    console.log("Searching for '" + search_string + "'");    

    let search_type = "";
    let search_for_tags = [];
    let tags_string = "";

    if (search_string.startsWith("tag:")) {
        // tag(s)
        search_type = "tags";

        let parts = search_string.split(" ");
        parts.forEach(part => {
            if (part.startsWith("tag:")) {
                search_for_tags.push(part.replace("tag:", ""));
                if (tags_string.length > 0) tags_string += "+";
                tags_string += part.replace("tag:", "");
            }
        });
    } else {
        // string
        search_type = "string";
    }

    let search_results = [];
    switch (search_type) {
        case "tags":
            res.send({ redirect: "/tagged/" + tags_string })
            break;
        case "string":
            res.send({ redirect: "/search?search=" + search_string + "&items=10"})
            break;
    }

});

module.exports = router;

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};