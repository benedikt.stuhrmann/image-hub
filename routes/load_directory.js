const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const path = require("path");
const Post = require("../models/post");
const utils = require("../utils");
const fs = require("fs");

router.post("/", (req, res, next) => {
    let dir = req.body.directory.trim();
    console.log("Loading " + dir);    

    let files = fs.readdirSync("./resources/local_images/" + dir);

    files.forEach( (link, index) => {
        let extension = utils.getFileExtension(link);
        if (!utils.isSupportedExtension(extension)) {
            files.splice(index, 1);
        }
    });

    res.status(200).json({links: files});

});

module.exports = router;