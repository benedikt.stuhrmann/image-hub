const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const Post = require("../models/post");
const fs = require("fs");
const utils = require("../utils");
const images_path = "resources/local_images/_auto";

router.get("/", (req, res, next) => {
    res.render("auto_create_posts", {});
});

// auto create
router.get("/create", (req, res, next) => {
    autoCreatePosts();
    res.redirect("/tagged/todo");
});

// edit auto created
router.get("/edit", (req, res, next) => {
    let where = {
        tags: { $all: ["todo"] }
    };

    Post.findOne(where)
        .exec()
        .then(post => {
            res.redirect("/edit-post/" + post._id + "?tag=todo");
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });
});

function autoCreatePosts(full_folder_path = images_path) {
    let dir_levels = full_folder_path.split("/")
    let folder_name = dir_levels[dir_levels.length - 1];
    let folder_path = full_folder_path.replace(folder_name, "");

    console.log("Processing folder " + folder_path + folder_name);

    // get all items in that folder
    let items_in_folder = [];
    fs.readdirSync(full_folder_path).forEach(item => {
        items_in_folder.push(item);
    });

    if (!items_in_folder.length) {
        console.log("No items found in folder '" + full_folder_path + "'.")
        return;
    }

    let contains_image  = false;
    let contains_folder = false;

    // loop through folder items
    items_in_folder.forEach( (item, index) => {
        if (fs.statSync( full_folder_path + "/" + item).isDirectory()) {
            // item is a directory
            contains_folder = true;
        } else {
            // item is a file
            contains_image = true;
        }
    });

    if (contains_folder && contains_image) {
        // contains folder(s) and image(s) => process the folders and create a post out of each image
        items_in_folder.forEach( (folder_or_image, index) => {
            if (fs.statSync( full_folder_path + "/" + folder_or_image).isDirectory()) {
                // item is a folder
                autoCreatePosts(full_folder_path + "/" + folder_or_image);
            } else {
                // item is a file
                createPostWithImages(folder_path, folder_name, [folder_or_image]);
            }
        });
    } else if (contains_folder) {
        // contains only folders => process those
        items_in_folder.forEach( (folder, index) => {
            autoCreatePosts(full_folder_path + "/" + folder);
        });
    } else {
        // contains only images => create a post out of all those images (unless the folder name is not numeric!)
        if (isNaN(folder_name)) {
            // folder name is not a number => currently in a base folder => create a post for each image
            items_in_folder.forEach( (image, index) => {
                createPostWithImages(folder_path, folder_name, [image]);
            });
        } else {
            createPostWithImages(folder_path, folder_name, items_in_folder);
        }
    }
    
}; // end autoCreatePosts()

async function createPostWithImages(folder_path, folder_name, images) {
    console.log("---------------------");
    console.log("Creating post with " + images.length + " images.");
    
    let hashtags = getHashtaghs(folder_path + folder_name);

    // move files
    images = moveFiles(folder_path, folder_name, images);

    let rotations = "";
    for (let index = 0; index < images.length; index++) {
        rotations += "0,"
    }
    rotations.length = rotations.length - 1;
    
    let post = utils.createPostObject(null, null, images, rotations, hashtags);
    
    await post.save()
      .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });

    console.log("---------------------");
} // end createPostWithImages()

function moveFiles(folder_path, folder_name, images) {
    let from_dir = folder_path + folder_name;
    let to_dir = folder_path.replace("_auto/", "") + folder_name;
    console.log(to_dir);

    // if the folder-name is numeric => check if it already exists
    if (!isNaN(folder_name)) {
        //console.log("Checking if folder " + to_dir + " already exists");
        let folder_exists = fs.existsSync(to_dir + "/");

        // if the folder already exists
        if (folder_exists) {
            //console.log("Folder with that name already exists.");
            folder_name = nextFreeFolderNumber(to_dir.replace(folder_name, ""));
            //console.log("Changing name of folder to " + folder_name);
            to_dir = folder_path.replace("_auto/", "") + folder_name;
            //console.log("New folder path to copy to is " + to_dir);
        }
    }
    
    // name images correctly for saving them in the db and then move them to the correct location
    let image_base_path = to_dir.replace("resources", "");
    images.forEach( (image, index) => {
        images[index] = image_base_path + "/" + image;
        
        // copy images to new folder
        let old_path = from_dir + "/" + image;
        let new_path = to_dir + "/" + image;
        
        utils.move(old_path, new_path, (error) => {
            if (error) {
                console.error(error);
            } else {
                //console.log("Moved image from " + old_path + " to " + new_path);
            }
        });
    });

    return images;
} // end moveFiles()

/**
 * Generates the preset hashtags for auto created posts.
 * @param {*} dir Directory path.
 */
function getHashtaghs(dir) {
    const blacklist = ["resources", "local_images", "_auto"];

    let hashtags = dir.split("/");   
    // filter out empty elements from the hashtags
    hashtags = hashtags.filter(function (hashtag) {
        return (hashtag != false) && (!blacklist.includes(hashtag));
    });
    hashtags.push("todo");

    return hashtags;
} // end getHashtags

/**
 * Returns the lowest unused folder number.
 * @param {*} dir Directory to check
 */
function nextFreeFolderNumber(dir) {
    let lowest_free_number = 1;
    while(fs.existsSync(dir + "/" + lowest_free_number)) {
        lowest_free_number ++;
    }
    return lowest_free_number;
} // end nextFreeFolderNumber









module.exports = router;