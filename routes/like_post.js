const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const path = require("path");
const Post = require("../models/post");

router.post("/", (req, res, next) => {
    let post_id = req.body.post_id;
    let update_type = req.body.likes;
    console.log(update_type + " post with id " + post_id);

    let like_count = 0;

    // get post from database
    Post.findOne({ _id: post_id})
        .exec()
        .then(post => {
            // get its current likes
            like_count = post.likes;

            // increase/decrease the likes
            switch (update_type) {
                case "like":
                    like_count += 1;
                    break;
                case "unlike":
                    like_count -= 1;
                    break;
            }

            // update new like count in database
            Post.updateOne(
                {
                    _id : post_id
                },
                {
                    likes: like_count
                }, 
                () => {
                    // after update, send response with new like count to update the page
                    res.status(200).json({likes: like_count});
                }
            );

        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });

});

module.exports = router;