const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const path = require("path");
const Post = require("../models/post");
const utils = require("../utils");

router.get("/", (req, res, next) => {
    console.log('nope');
    let all_tags = new Map();
    let image_count = 0;
    let video_count = 0;

    Post.find()
        .exec()
        .then(posts => {
            posts.forEach( (post) => {
                // get tag-statistics 
                post.tags.forEach( (tag) => {
                    if (!all_tags.has(tag)) {
                        // first occurence of the tag
                        all_tags.set(tag, 1);
                    } else {
                        // tag already exists in map
                        let times = all_tags.get(tag);
                        times = times * 1 + 1;
                        all_tags.set(tag, times);
                    }
                });

                // get element-statistics
                post.content.forEach( (content_element) => {
                    if (content_element.type == "image") {
                        image_count++;
                    } else {
                        video_count++;
                    }
                });
            });

            all_tags = new Map([...all_tags.entries()].sort((a, b) => b[1] - a[1]));

            res.render("statistics", {
                tags:           Array.from(all_tags),
                posts:          posts.length,
                elements_total: image_count + video_count,
                images:         image_count,
                videos:         video_count
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });

});

module.exports = router;