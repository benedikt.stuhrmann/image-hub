const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const path = require("path");
const Post = require("../models/post");
const utils = require("../utils");

router.get("/", (req, res, next) => {
    let all_tags = new Map();

    Post.find()
        .exec()
        .then(posts => {
            posts.forEach( (post) => {
                // get tag-statistics 
                post.tags.forEach( (tag) => {
                    if (!all_tags.has(tag)) {
                        // first occurence of the tag
                        all_tags.set(tag, 1);
                    } else {
                        // tag already exists in map
                        let times = all_tags.get(tag);
                        times = times * 1 + 1;
                        all_tags.set(tag, times);
                    }
                });
            });

            all_tags = new Map([...all_tags.entries()].sort((a, b) => b[1] - a[1]));
            all_tags = Array.from(all_tags).map( (entry) => {
                return entry[0];
                /*return { 
                    tag: entry[0], 
                    times: entry[1]
                };*/
            });

            res.json(all_tags);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });

});

module.exports = router;