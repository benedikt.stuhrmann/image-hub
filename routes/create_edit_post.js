const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const utils = require("../utils");
const Post = require("../models/post");
const path = require('path')

const multer  = require("multer");
const MediaHandler = require("../media_handler");
const mediaHandler = new MediaHandler();
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, "resources/local_images/uploads");
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + path.extname(file.originalname));
    }
  })
const upload = multer({ storage: storage });

// create
router.get("/", (req, res, next) => {
    let post = new Post({
        _id:     null,
        title:   "",
        reflink: "",
        content: [],
        tags:    []
    });

    res.render("create_edit_post", { post: post, multi: false });
});

// edit
router.get("/*", (req, res, next) => {

    // edit multiple posts?
    let multi = (req.query.tag && req.query.tag != null) ? req.query.tag : false;
    console.log(multi);

    post_id = req.originalUrl.replace("/edit-post/", "");
    console.log(post_id);

    let query_start = post_id.indexOf("?");
    if (query_start !== -1) {
        post_id = post_id.substring(0, query_start);
    }

    console.log(post_id);

    Post.findOne({_id: post_id})
        .exec()
        .then(post => {
            res.render("create_edit_post", {
                post: post,
                multi: multi
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });

});

router.post("/upload-media", upload.array("media-items"), async (req, res, next) => {
    let links = req.files.map( (file) => utils.imagePath(file.path));
    res.status(200).json({links: links});
});

router.post("/", (req, res, next) => {
    let data = req.body;

    let post = utils.createPostObject(data.title, data.reflink, data.content, data.rotations, data.tags);

    if (data.id != null && data.id != "") {
        // UPDATE EXISTING
        Post.updateOne(
            {
                _id : data.id
            },
            {
                title:   post.title,
                reflink: post.reflink,
                content: post.content,
                tags:    post.tags,
                updated: new Date()
            }, 
            () => {
                let multi = (req.body.multi && req.body.multi != null && req.body.multi != "false") ? req.body.multi : false;
                if (multi) {
                    // edit another random post with the multi-tag
                    res.redirect("/auto-create-posts/edit");
                } else {
                    // default behaviour
                    Post.find()
                        .sort({created:-1})
                        .exec()
                        .then(posts => {
                            mediaHandler.deleteUnusedUploads();
                            res.redirect("/");
                        })
                        .catch(err => {
                            console.log(err);
                            res.status(500).json({error: err});
                        });
                }
            }
        );

    } else {
        // CREATE NEW       
        post.save()
            .then(result => {
                Post.find()
                    .sort({created:-1})
                    .exec()
                    .then(posts => {
                        mediaHandler.deleteUnusedUploads();
                        res.redirect("/");
                    })
                    .catch(err => {
                        console.log(err);
                        res.status(500).json({error: err});
                    });
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({
                    error: err
                });
            });
    }
    
});

module.exports = router;