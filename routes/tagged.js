const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const path = require("path");
const Post = require("../models/post");
const utils = require("../utils");

router.get("/", (req, res, next) => {
    let search = req.originalUrl.replace("/tagged/", "");
    let order = req.query["order-by"];
    let order_query = utils.orderQuery(order);
    
    // cut possible query string
    query_start = search.indexOf("?");
    if (query_start != -1) {
        search = search.substr(0, query_start);
    }

    let item_amount = (req.query.items && req.query.items != null) ? req.query.items : 10;

    let search_for_tags = search.split("+");

    Post.find({ tags: { $all: search_for_tags } }) // find all posts that have all the elements in the search_for_tags array 
        .sort(order_query)
        .exec()
        .then(posts => {
            console.log("Found " + posts.length + " posts for following tag-combination: " + search_for_tags);
            let posts_to_show = utils.getPostsForPage(req, posts, item_amount);
            res.render("index", {
                search_string: search_for_tags,
                page_info:     posts_to_show.info,
                posts:         posts_to_show.posts,
                order:         order
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });

});

module.exports = router;