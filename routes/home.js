const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const path = require("path");
const Post = require("../models/post");
const utils = require("../utils");

router.get("/", (req, res, next) => {
    let item_amount = (req.query.items && req.query.items != null) ? req.query.items : 10;
    let order = req.query["order-by"];
    let order_query = utils.orderQuery(order);

    Post.find()
        .sort(order_query)
        .exec()
        .then(posts => {
            let posts_to_show = utils.getPostsForPage(req, posts, item_amount);
            res.render("index", {
                search_string: null,
                page_info:     posts_to_show.info,
                posts:         posts_to_show.posts,
                order:         order
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });

});

module.exports = router;