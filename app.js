const express = require("express");
const body_parser = require("body-parser");
const express_layouts = require("express-ejs-layouts");
const app = express();
const mongoose = require("mongoose");
const db_path = "mongodb://root:example@localhost:27017/image_hub?authSource=admin" //"mongodb://localhost:27017/image_hub";

app.use(body_parser.urlencoded({extended: true})); // body_parser.json()
app.use(body_parser.json());
app.use(express.static(__dirname + "/resources"));
app.set("view engine", "ejs");
app.use(express_layouts);

const home_routes = require("./routes/home");
const create_edit_post_routes = require("./routes/create_edit_post");
const auto_create_posts_routes = require("./routes/auto_create_posts");
const delete_post_routes = require("./routes/delete_post");
const tagged_routes = require("./routes/tagged");
const search_routes = require("./routes/search");
const like_post_routes = require("./routes/like_post");
const statistics_routes = require("./routes/statistics");
const load_dir_routes = require("./routes/load_directory");
const tagslist_routes = require("./routes/tagslist");

//mongoose.connect(db_path, { useNewUrlParser: true });

mongoose.connect(db_path, {useNewUrlParser: true});

// listen for incoming requests and route them
app.use("/create-post", create_edit_post_routes); // create Post
app.use("/create-post/upload-media", create_edit_post_routes); // upload media
app.use("/edit-post", create_edit_post_routes); // edit Post
app.use("/edit-post/*", create_edit_post_routes); // edit Post
app.use("/edit-todo-post/*", create_edit_post_routes); // edit todo Post
app.use("/auto-create-posts/", auto_create_posts_routes); // auto create Posts
app.use("/delete-post/*", delete_post_routes); // delete Post
app.use("/tagged/*", tagged_routes); // tagged
app.use("/search*", search_routes); // search
app.use("/like-post*", like_post_routes); // like Post
app.use("/statistics*", statistics_routes); // statistics
app.use("/load-dir*", load_dir_routes); // load directory
app.use("/tagslist*", tagslist_routes); // list of all tags
app.use("/", home_routes); // home

// 404
app.use((req, res, next) => {
    console.log("404: Route not Found - " + req.originalUrl);

    res.render("error404", {
        url: req.originalUrl
    });
});

module.exports = app;